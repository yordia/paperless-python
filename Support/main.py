
import time

from Definitions.Reporte.importar_vistas_sql import GeneraReporteExcel
from Definitions.config import Config
from Definitions.driver_chrome import DriverChrome
from Definitions.Log.log_insertar_tabla_BD import InsertarTablaLog
from Definitions.Log.log_descarga_masivo import DescargaLogMasivo
from Definitions.driver_reutilizar import ReutilizarDriver
from Definitions.Log.log_guardar_tickets_impresos_BD import GuardarTicketsImpresosLog
from Page.page_autenticacion_doble_factor import AutenticacionDobleFactorPage
from Page.pdf_extraer_datos_insertar_BD import ExtraerDatosPdf
from Page.page_grilla_resultados import GrillaResultadosPage
from Page.page_login import LoginPage
from Page.page_filtros import FiltrosPage

def main():
    config = Config()
    descargar_logs_masivos = DescargaLogMasivo(config)
    lista_ruta_windows = descargar_logs_masivos.tiempoEjecucion()
    #lista_ruta_windows = ['B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM005.LOG']
    """
    lista_ruta_windows = [
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM001.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM002.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM003.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM004.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM005.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM006.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM007.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM008.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM009.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM010.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM011.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM012.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM013.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM014.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM015.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM016.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM017.LOG',
        'B:\\ncr\\REPROCESAR\\01copiar-logs-un-dia\\L_JVM018.LOG']
    """

    #Tiempo q demora procesar los 18 logs e insertarlos a la BD
    tiempoEjecucion(config,lista_ruta_windows)

    #SE INICIA LOGIN EN CHROME
    driver_chrome = DriverChrome(config)
    reutilizar_driver = ReutilizarDriver(driver_chrome.driver, config)
    login_page = LoginPage(driver_chrome.driver, config,reutilizar_driver)
    autenticacion_doble_page = AutenticacionDobleFactorPage(driver_chrome.driver,reutilizar_driver)

    filters_page = FiltrosPage(driver_chrome.driver, config, reutilizar_driver)
    grilla_resultados_page = GrillaResultadosPage(driver_chrome.driver, config, reutilizar_driver)
    extraer_datos_pdf = ExtraerDatosPdf(config)


    # Abre la página de inicio de sesión
    login_page.open_login_page()

    #ir a pagina login no existe,de momento lo comentamos
    #login_page.ir_pagina_login()

    # Inicia sesión
    #login_page.login()
    login_page.login_antiguo()
    # login_page.verificar_pagina_inicio() ERROR AL ENCONTRAR ELEMENTO, YA Q LA PAGINA DEMORA EN CARGAR

    #time.sleep(config.tiempo_espera_paginas_web_lentas)
    #autenticacion_doble_page.saltar_autenticacion_doble()

    time.sleep(config.tiempo_espera_paginas_web_lentas)
    filters_page.pagina_inicio()

    time.sleep(config.tiempo_espera_paginas_web_rapidas)
    filters_page.ingresar_pagina_gestion_documental()

    time.sleep(config.tiempo_espera_paginas_web_rapidas)
    filters_page.ingresar_pagina_filtros()
    #input("detenerrrr")

    time.sleep(config.tiempo_espera_paginas_web_rapidas)
    #ejecutamos extraer_datos_pdf
    lista_tabla_web_completa = grilla_resultados_page.descarga_pdf()
    # este metodo inserta bd y actualiza bd
    extraer_datos_pdf.principal(lista_tabla_web_completa)
    print("Título de la página después del inicio de sesión:", driver_chrome.driver.title)

    #genera reporte excel
    generar_reporte=GeneraReporteExcel(config)
    generar_reporte.generar_reporte()

    # Mantener un bucle infinito para evitar que el navegador se cierre automáticamente
    var = input("FORZAR QUE CHROME NO SE CIERRE,Presiona 0 para salir")
    while var != '0':
        var = input("FORZAR QUE CHROME NO SE CIERRE,Presiona 0 para salir")
        if var=='0':
            break
    # Cierra el navegador al finalizar
    reutilizar_driver.driver.quit()
    #"""

def tiempoEjecucion(config,lista_ruta_windows):
    start_time = time.time()

    obtener_tickets_impresion = GuardarTicketsImpresosLog(config)
    obtener_tickets_impresion.procesar_varios_logs(lista_ruta_windows)
    insertar_log = InsertarTablaLog(config)
    insertar_log.principal()

    end_time = time.time()
    # Cálculo del tiempo de ejecución
    execution_time = end_time - start_time
    print("tiempo de procesar logs e insertar a BD es de", execution_time, "segundos.")


if __name__ == "__main__":
    main()
    """
    config=Config()
    generar_reporte=GeneraReporteExcel(config)
    generar_reporte.generar_reporte()
    """
    #input("Presiona Enter para salir del cmd...")
