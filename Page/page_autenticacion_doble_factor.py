from selenium.webdriver.common.by import By
class AutenticacionDobleFactorPage:
    def __init__(self,driver,reutilizar_driver):
        self.driver=driver
        self.reutilizar_driver=reutilizar_driver
    def saltar_autenticacion_doble(self):
        boton_no_autenticacion_doble_field = self.driver.find_element(By.XPATH,"/html/body/div/div/div/div/button[1]")
        self.reutilizar_driver.esperando_elemento_html(boton_no_autenticacion_doble_field)
        boton_no_autenticacion_doble_field.click()