from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

class LoginPage:
    def __init__(self, driver, config,reutilizar):
        self.driver = driver
        self.config = config
        self.reutilizar_driver = reutilizar
        self.login_url = config.login_url
        self.login_url_antiguo_sin_ssl = config.login_url_antiguo_sin_ssl

    def open_login_page(self):
        self.driver.get(self.login_url_antiguo_sin_ssl)

    def ir_pagina_login(self):
        enlace_iniciar_sesion_field=self.driver.find_element(By.XPATH,"/html/body/table/tbody/tr[5]/td/table/tbody/tr[8]/td/a")
        self.reutilizar_driver.esperando_elemento_html(enlace_iniciar_sesion_field)
        enlace_iniciar_sesion_field.click()
    def login(self):
        username_field = self.driver.find_element(By.XPATH,"/html/body/div/div/div[1]/div/div/form/div[1]/div/input")
        password_field = self.driver.find_element(By.XPATH,"/html/body/div/div/div[1]/div/div/form/div[2]/div/input")
        username_field.send_keys(self.config.username)
        password_field.send_keys(self.config.password)
        password_field.send_keys(Keys.RETURN)

    def login_antiguo(self):

        ruc_field = self.driver.find_element(By.XPATH,
                                             "/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[2]/td[2]/input")
        username_field = self.driver.find_element(By.XPATH,
                                                  "/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[3]/td[2]/input")
        password_field = self.driver.find_element(By.XPATH,
                                                  "/html/body/table/tbody/tr[2]/td/form/table/tbody/tr[4]/td[2]/input[1]")

        ruc_field.send_keys(self.config.ruc)
        username_field.send_keys(self.config.username_antiguo)
        password_field.send_keys(self.config.password_antiguo)
        password_field.send_keys(Keys.RETURN)

    #Verificamos q la pagina inicio cargo correctamente y asi para cada pagina..
    def verificar_pagina_inicio(self):
        menu_gestion_documental_field = self.driver.find_element(By.XPATH,"/html/body/table[1]/tbody/tr[2]/td/div/a[3]/img")
        WebDriverWait(self.driver, self.config.tiempo_espera_global).until(EC.element_to_be_clickable(menu_gestion_documental_field))


