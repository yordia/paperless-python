from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

class FiltrosPage:
    def __init__(self, driver, config, reutilizar):
        self.driver = driver
        self.config = config
        self.caja_a_seleccionar=config.caja
        self.reutilizar_driver=reutilizar

    def pagina_inicio(self):
        #pagina inicio
        menu_gestion_documental_field = self.driver.find_element(By.XPATH,"/html/body/table[1]/tbody/tr[2]/td/div/a[3]/img")
        self.reutilizar_driver.esperando_elemento_html(menu_gestion_documental_field)
        menu_gestion_documental_field.click()
    def ingresar_pagina_gestion_documental(self):
        # Pagina donde aparece opcion gestion documental
        enlace_documentos_emitidos_field = self.driver.find_element(By.XPATH,
                                                                    "/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/table/tbody/tr[1]/td[1]/a/img")
        self.reutilizar_driver.esperando_elemento_html(enlace_documentos_emitidos_field)
        enlace_documentos_emitidos_field.click()

    def ingresar_pagina_filtros(self):

        check_todos_tipo_documentos_field=self.driver.find_element(By.XPATH,"/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/div/form[1]/table/tbody/tr[3]/td[2]/table/tbody/tr[1]/td/input[1]")
        self.reutilizar_driver.esperando_elemento_html(check_todos_tipo_documentos_field)
        check_todos_tipo_documentos_field.click()

        check_todos_estados_documentos_field=self.driver.find_element(By.XPATH,"/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/div/form[1]/table/tbody/tr[3]/td[2]/table/tbody/tr[3]/td/input[1]")
        self.reutilizar_driver.esperando_elemento_html(check_todos_estados_documentos_field)
        check_todos_estados_documentos_field.click()

        caja_fecha_inicio_field = self.driver.find_element(By.ID,"fchemiini")
        self.reutilizar_driver.esperando_elemento_html(caja_fecha_inicio_field)
        caja_fecha_inicio_field.send_keys(self.config.fecha_inicio)

        caja_fecha_fin_field = self.driver.find_element(By.ID, "fchemifin")
        self.reutilizar_driver.esperando_elemento_html(caja_fecha_fin_field)
        caja_fecha_fin_field.send_keys(self.config.fecha_fin)

        #busca el combo de tiendas
        combo_tienda_field = self.driver.find_element(By.ID,"txtLocal")
        self.reutilizar_driver.esperando_elemento_html(combo_tienda_field)
        # Crear un objeto Select a partir del elemento combo
        select = Select(combo_tienda_field)
        # Seleccionar una opción por su texto visible
        select.select_by_visible_text(self.config.tienda)
        # Verificar que la opción seleccionada es la correcta
        elemento_seleccionado = select.first_selected_option
        assert elemento_seleccionado.text == self.config.tienda, "La tienda seleccionada no es la correcta"
        # print('TIENDA SELECCIONADA:',elemento_seleccionado.text)
        #Enter
        combo_tienda_field.send_keys(Keys.RETURN)
        """
        combo_tienda_field.send_keys(Keys.ARROW_UP)
        combo_tienda_field.send_keys(Keys.RETURN)
        combo_tienda_field.send_keys(Keys.ARROW_DOWN)
        """
        if self.config.filtra_caja:
            # busca el combo de caja
            combo_caja_field = self.driver.find_element(By.ID, "txtCaja")
            self.reutilizar_driver.esperando_elemento_html(combo_caja_field)
            # Crear un objeto Select a partir del elemento combo
            select = Select(combo_caja_field)

            # Seleccionar una opción por su texto visible
            select.select_by_visible_text(self.caja_a_seleccionar)
            print(self.caja_a_seleccionar)

            # Verificar que la opción seleccionada es la correcta
            elemento_seleccionado = select.first_selected_option
            assert elemento_seleccionado.text == self.caja_a_seleccionar, "La caja seleccionada no es la correcta"
            # print('CAJA SELECCIONADA:',elemento_seleccionado.text)


        boton_buscar_field =self.driver.find_element(By.XPATH, '//input[@value="Buscar"]')
        self.reutilizar_driver.esperando_elemento_html(boton_buscar_field)
        boton_buscar_field.click()

    def verificar_pagina_filtros(self):
        pass
