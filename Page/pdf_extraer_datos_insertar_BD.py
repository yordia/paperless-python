import os
import time

import pypdfium2 as pdfium

from Definitions.query_reutilizable import ReutilizarQuery
from Definitions.querys import DatabaseManager

#serie_correlativo = sys.argv[1]
#tipo_doc = sys.argv[2]

#se podria verificar contando la cantidad de archivos descargados vs la cantidad de comprobantes de la grilla
class ExtraerDatosPdf:
    #soles_pagados es = TOTAL CONTADO
    def __init__(self, config):
        #constantes importantes:
        self.config = config
        self.ruta_descarga_pdf=config.ruta_descarga_pdf
        self.tiempo_espera_ultimo_pdf_descargado=config.tiempo_espera_ultimo_pdf_descargado

        self.ruc=config.nombre_pdf_ruc
        self.nombre_pdf_separacion = config.nombre_pdf_separacion
        self.nombre_pdf_digito_boleta=config.nombre_pdf_digito_boleta
        self.nombre_pdf_digito_factura=config.nombre_pdf_digito_factura
        self.nombre_pdf_digito_nota_credito=config.nombre_pdf_digito_nota_credito
        self.nombre_pdf_serie_boleta=config.nombre_pdf_serie_boleta

        #borrando tabla BD
        self.tabla_web_sovos=config.tabla_web_sovos
        reutilizar_query = ReutilizarQuery()
        reutilizar_query.borrar_tabla(config, config.tabla_web_sovos)

    # devuelve directorio csv
    #if cantidad_paginas==0: no llamar al resto de scripts

    def principal(self,lista_tabla):
        #En caso no exista fecha emision en la nota credito, lo insertamos desde la tabla web, por ello le enviamos la tabla web
        self.insertar_bd(lista_tabla)
        self.procesar_y_actualizar(lista_tabla)
    def procesar_pdf_a_texto(self,ruta_pdf):
        text=''
        pdf = pdfium.PdfDocument(ruta_pdf)
        for i in range(len(pdf)):
            page = pdf.get_page(i)
            textpage = page.get_textpage()
            # para eliminar espacios del inicio y fin
            text += textpage.get_text().strip()
            # text += "\n"
            [g.close() for g in (textpage, page)]
        pdf.close()

        # trae por defecto /r cuando deberia traer /n (al menos a si funciona en esta libreria)
        lista = text.split('\r\n')
        # orden correcto primero r luego n (de esta manera la lista no tendra \r o \n')

        # Variables iniciales
        entro_tipo_comprobante = True
        tipo_comprobante =''
        op_gravada =''
        igv = ''
        importe_total =''

        serie_correlativo =''
        fecha_emision = ''

        redondeo = ''
        soles_pagados = ''
        op_inafecta = ''

        # Palabras clave para búsqueda
        comprobante_keywords = ['BOLETA DE VENTA ELECTRONICA', 'FACTURA ELECTRONICA', 'NOTA DE CREDITO ELECTRONICA']
        op_gravada_keyword = 'OP. GRAVADA'
        igv_keyword = 'I.G.V.'
        importe_total_keyword = 'IMPORTE TOTAL S/'

        serie_correlativo_keywords = ['F104 Nº', 'B104 Nº']
        fecha_emision_keyword = 'Fecha de Emisión:'

        redondeo_keyword = 'REDONDEO'
        soles_pagados_keyword = 'TOTAL CONTADO'
        op_inafecta_keyword = 'OP. INAFECTA'

        for e in lista:
            if entro_tipo_comprobante and any(keyword in e for keyword in comprobante_keywords):
                tipo_comprobante = e
                #SOLO PARA TIPO FACTURA ya q es el unico q al imprimir en sovos na trae texto 'DE VENTA'
                if tipo_comprobante=='FACTURA ELECTRONICA':
                    tipo_comprobante='FACTURA DE VENTA ELECTRONICA'

                entro_tipo_comprobante = False
            elif op_gravada_keyword in e:
                op_gravada = e
            elif igv_keyword in e:
                igv = e
            elif importe_total_keyword in e:
                importe_total = e
            elif redondeo_keyword in e:
                redondeo = e
            elif soles_pagados_keyword in e:
                soles_pagados = e
            elif op_inafecta_keyword in e:
                op_inafecta = e
            elif any(keyword in e for keyword in serie_correlativo_keywords):
                serie_correlativo = e
            elif fecha_emision_keyword.lower() in e.lower():
                print('linea del pdf donde esta fecha emision',e)
                fecha_emision = e


            #SI FECHA EMISION ES VACIO, COGER LA FECHA DE LA GRILLA
            #segundo caja
        #(op_gravada,'\n',igv,'\n', importe_total,'\n', redondeo,'\n', soles_pagados,'\n', op_inafecta)
        return tipo_comprobante.strip(),serie_correlativo.strip(),op_gravada.strip(), igv.strip(), importe_total.strip(), redondeo.strip(), soles_pagados.strip(), op_inafecta.strip(),fecha_emision.strip()
    def extraer_data_pdf(self):
        time.sleep(self.tiempo_espera_ultimo_pdf_descargado)
        nombres_pdfs=self.lista_pdf()
        #print(nombres_pdfs)
        tabla_resultados=[]
        for a, pdf_file in enumerate(nombres_pdfs, start=1):
            nueva_lista =self.procesar_pdf_a_texto(pdf_file)
            tabla_resultados.append(nueva_lista)
        #for lista in tabla_resultados:
        #    print(" ".join(map(str, lista)))
        return tabla_resultados

    def procesar_e_insertar(self,fila_n,tabla_web):
        #print('arrreglo: ',fila_n)
        pdf_tipo_comprobante = fila_n[0]
        pdf_serie_correlativo = fila_n[1]
        pdf_op_gravada = self.obtenerUltimaPosicion(fila_n[2])
        pdf_igv = self.obtenerUltimaPosicion(fila_n[3])
        #inicializamos variable para evitar error

        if pdf_igv=='':
            pdf_igv='NO_EXISTE_IGV'
        pdf_importeTotal = self.obtenerUltimaPosicion(fila_n[4])
        pdf_redondeo = self.obtenerUltimaPosicion(fila_n[5])
        if pdf_redondeo=='':
            pdf_redondeo='NO_EXISTE_REDONDEO'
        pdf_soles_pagados = self.obtenerUltimaPosicion(fila_n[6])
        if pdf_soles_pagados=='':
            #se necesita por q de lo contrario, al hacer update al parecer el NONE DESAPARECE(pendiente probar???)
            pdf_soles_pagados='NO_EXISTE_TOTAL_CONTADO'

        pdf_op_inafecta = self.obtenerUltimaPosicion(fila_n[7])
        #del antepenultimo elemento hasta el ultimo elemento [-3:]

        if fila_n[8]=='NO_EXISTE_FECHA_EMISION':
            pdf_fecha_emision =fila_n[8]
        else:
            # toma la posicion 8
            #despues se tomara los ultimos 19 caracteres de la cadena.
            pdf_fecha_emision =fila_n[8][-19:]

        cadena =pdf_tipo_comprobante+'+'+pdf_serie_correlativo+'+'+ pdf_op_gravada + '+' + pdf_igv + '+' + pdf_importeTotal + '+' + pdf_redondeo + '+' + pdf_soles_pagados + '+' + pdf_op_inafecta+'+'+pdf_fecha_emision+'+'
        # Uso de la clase
        db_manager = DatabaseManager(self.config)
        db_manager.connect()

        # Inserción de datos
        #lista
        insert_columns = ['num_caja','tipo_doc','serie_correlativo','fecha_emision','op_gravada','igv','importe_total','estado_comprobante','redondeo','soles_pagados','op_inafecta']
        #tupla no se puede editar
        #None para inertar null en BD
        grilla_web_num_caja=None
        grilla_web_estado_comprobante=None
        #pdf_soles_pagados a veces es vacio ''

        # convertimos serie correlativo a formato de tabla web
        folio_convertido_a_web = self.convertir_folio_pdf_a_web(pdf_serie_correlativo)
        filas_encontradas = [lista for lista in tabla_web if folio_convertido_a_web in lista]
        if len(filas_encontradas)>=2:
            print('POSIBLE_FOLIO_REPETIDO')

        #para casos con nota credito, DONDE FECHA EMISION ES IGUAL A VACIO ''
        if pdf_fecha_emision=='':
            #posicion 1 indica fecha emision de la web
            #solo se tomara el primer resultado en caso folio este duplicado ya a la busqueda en la lista es por folio.
            pdf_fecha_emision=filas_encontradas[0][1]
            #print('AQUIII:',pdf_fecha_emision)
            pdf_fecha_emision = self.convertir_fecha_web_a_fecha_pdf(pdf_fecha_emision)

        insert_values = (grilla_web_num_caja,pdf_tipo_comprobante,folio_convertido_a_web,pdf_fecha_emision,pdf_op_gravada,pdf_igv,pdf_importeTotal,grilla_web_estado_comprobante,pdf_redondeo,pdf_soles_pagados,pdf_op_inafecta)
        # si se inserta vacio (no null) significa q no existe en PDF
        rpta_bd=db_manager.insert_data(self.tabla_web_sovos, insert_columns, insert_values)

        if rpta_bd:
            pass
            #print('SE INSERTO EXITOSAMENTE')
        else:
            print('ERROR AL INSERTAR TABLA_PDF')
        # cerrar siempre conexion
        db_manager.disconnect()

        return cadena
    def procesar_y_actualizar(self,tabla_web):
        #2 OPCION DE IMPRIMIR lista de listas
        for fila in tabla_web:
            print("||".join(map(str, fila)))
            #CONDICION WHERE se remplazara DE fecha emision  por importe total de pdf, de este modo solo encontrara uno a no ser q se duplique serie y monto total
            caja_tabla_web=fila[0]
            estado_tabla_web=fila[2]

            _fecha_emision=fila[1].split('-')
            _fecha_emision.reverse()
            fecha_emision_tabla_web='-'.join(_fecha_emision)
            fecha_emision_tabla_web='%'+fecha_emision_tabla_web.strip()+'%'

            serie_correlativo_tabla_web = self.convertir_folio_pdf_a_web(fila[4])

            self.actualizar_bd(caja_tabla_web,estado_tabla_web,fecha_emision_tabla_web,serie_correlativo_tabla_web)
    def insertar_bd(self,tabla_web):
        #lista de datos pdf o de las url ?
        var = self.extraer_data_pdf()
        tabla_datos=[]

        for fila in var:
            fila_n=self.procesar_e_insertar(fila,tabla_web)
            tabla_datos.append(fila_n)

        #print('DATOS INSERTADOS EN BD::')
    def actualizar_bd(self,caja_tabla_web,estado_tabla_web,fecha_emision_tabla_web,serie_correlativo_tabla_web):
        # Uso de la clase
        db_manager = DatabaseManager(self.config)
        db_manager.connect()

        # Definir los valores que quieres actualizar
        columns_to_update = ['num_caja', 'estado_comprobante']
        new_values = [caja_tabla_web, estado_tabla_web]

        # Definir la condición de actualización
        condition_column1 = 'fecha_emision'
        condition_value1 =fecha_emision_tabla_web
        condition_column2 = 'serie_correlativo'
        condition_value2 = serie_correlativo_tabla_web

        # Llamar al método update_data para realizar la actualización
        result = db_manager.update_data(self.tabla_web_sovos, columns_to_update, new_values, condition_column1, condition_value1,
                                        condition_column2, condition_value2)
        if result:
            pass
            #print("Actualización exitosa tabla pdffff")
        else:
            print("Error al actualizar")
        # cerrar siempre conexion
        db_manager.disconnect()

    def obtenerUltimaPosicion(self,arg):
        lista = arg.split(' ')
        if len(lista) <= 1:
            return ''
        else:
            return lista[-1]

    def lista_pdf(self):
        folder_path =self.ruta_descarga_pdf
        pdf_files = []

        for file in os.listdir(folder_path):
            if file.lower().endswith(".pdf") and os.path.isfile(os.path.join(folder_path, file)):
                pdf_files.append(os.path.join(folder_path, file))
        return  pdf_files
    def convertir_folio_pdf_a_web(self,folio):
        folio=folio.replace(' Nº ','-')
        return folio
    def convertir_fecha_web_a_fecha_pdf(self,fecha_web):
        fecha_year_mes_dia=fecha_web.split('-')
        fecha_year_mes_dia.reverse()
        cadena_fecha_convertida_a_pdf='-'.join(fecha_year_mes_dia)

        return cadena_fecha_convertida_a_pdf

