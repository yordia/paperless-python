import sys

from selenium.webdriver.common.by import By

class GrillaResultadosPage:

    def __init__(self, driver, config,reutilizar):
        self.driver = driver
        self.config = config
        self.reutilizar=reutilizar

    def obtener_total_registros (self):
        cantidad_paginas = 0
        residuo = 0

        texto_m_de_n_registros=self.driver.find_element(By.XPATH,"/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/table[2]/tbody/tr/td[1]")
        self.reutilizar.esperando_elemento_html(texto_m_de_n_registros)
        #se extrae solo texto, adicional se elimina espacios en blanco al inicio y al final.
        extracted_string=texto_m_de_n_registros.text.strip()
        arreglo_cadena=extracted_string.split(' ')
        print('Total de comprobantes:',arreglo_cadena[3])

        #doble division me devolvera el cociente entero
        # cociente entero
        cantidad_paginas = int(arreglo_cadena[3]) // 10
        # cantidad paginas en ultimo ciclo
        residuo = int(arreglo_cadena[3]) % 10

        if residuo == 0 and cantidad_paginas == 0:
            cantidad_paginas = 0
            print("No hay pdfs para descargar")
        elif int(arreglo_cadena[3])>=1 and int(arreglo_cadena[3])<=10:
            cantidad_paginas=1
            residuo = int(arreglo_cadena[3])
            print('SOLO HAY ENTRE 1 A 10 PAGINAS')
        elif residuo > 0:
            #para el caso en q sea 11,23,29,35 que no son multiplos de 10
            cantidad_paginas += 1
        elif residuo==0:
            #residuo es igual a 0
            #por tanto seran MULTIPLOS DE 10
            print("..INGRESA AQUI SOLO PARA MULTIPLOS DE 10")
        else:
            print('ESCENARIO NO CONTROLADO, ERROR NO CONTROLADO ')
            sys.exit(1)


        # Usamos cantidad_paginas en esta línea, a pesar de la advertencia
        return cantidad_paginas, residuo

    def descarga_pdf(self):
        cantidad_paginas, residuo = self.obtener_total_registros()
        print(cantidad_paginas, "cantidad_paginas")
        print(residuo, "residuo")

        xpath_no_varia = "/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/form[1]/table/tbody/tr["
        enlace_pdf_varia = "]/td[14]/a[3]"


        clic_n_paginas = self.calcular_clic_paginas(cantidad_paginas, residuo)
        numero_xpath_n_paginas = 0
        contador_auxiliar = 0

        lista_grilla=[]
        for i in range(1, cantidad_paginas + 1):
            if residuo == 0 and cantidad_paginas == 0:
                print('NO HAY PDF EN LA WEB SOVOS, PARA DESCARGAR')
                break
            else:
                # para casos pagina 10,20,30,40 ..
                numero_filas = 10
                #aqui la ultima pagina
                if i == cantidad_paginas:
                    clic_n_paginas = False
                    numero_filas = residuo
                    #solo si estamos en la ultima pagina y el residuo es 0
                    if residuo==0:
                        numero_filas=10
                    print("numeros filas igual a residuo o igual a 10")






            print("**** pagina: ", i)
            for j in range(numero_filas):
                rpta_fila=self.descargar_pdfs_pagina_n(j, xpath_no_varia, enlace_pdf_varia)
                lista_grilla.append(rpta_fila)

            numero_xpath_n_paginas,contador_auxiliar = self.actualizar_xpath_n_paginas(numero_xpath_n_paginas, contador_auxiliar)

            if clic_n_paginas:
                self.ir_a_pagina_n(numero_xpath_n_paginas)

        #imprimir encabeza tabla
        """
        encabezados=['num_caja','fecha_emision','estado','fecha_ingreso_hora','serie_correlativo']
        for encabezado in encabezados:
            print(encabezado, end="\t")  # Usar un tabulador como separador
        print()  # Cambiar de línea después de imprimir los encabezados
        # Imprimir filas
        for fila in lista_grilla:
            for elemento in fila:
                print(elemento,end='+')
            print()# Cambiar de línea después de imprimir la fila
        """

        #2 OPCION DE IMPRIMIR lista de listas
        #for lista in lista_grilla:
        #    print("##".join(map(str, lista)))

        return lista_grilla

    def calcular_clic_paginas(self, cantidad_paginas, residuo):
        #solo para el caso en q tenga 1 sola pagina
        if residuo >= 1 and residuo <= 9 and cantidad_paginas == 1:
            return False
        return True

    def actualizar_xpath_n_paginas(self, numero_xpath_n_paginas, contador_auxiliar):
        #logica fallo al separarlo en varios netodos, ya q por defecto contado auxiliar tomaba el valor 0. Al salir de su metodo volvio a su valor por defecto q es 0.
        #se debe devolver contador auxiliar, si no se devuelve contador auxiliar siempre volvera a ser 0. (Por tanto la logica fallara)

        numero_xpath_n_paginas += 1
        #ingresara solo la primera vez
        if numero_xpath_n_paginas == 10:
            contador_auxiliar += 1
            if contador_auxiliar == 1:
                numero_xpath_n_paginas = 2
                print(contador_auxiliar, "--ingreseeee 1111")
        if numero_xpath_n_paginas == 12 and contador_auxiliar >= 2:
            numero_xpath_n_paginas = 2
            print(contador_auxiliar,"--ingreseeee 2222")
        return numero_xpath_n_paginas,contador_auxiliar

    def ir_a_pagina_n(self, numero_xpath_n_paginas):
        enlace_pagina_n_xpath = (
                "/html/body/table[2]/tbody/tr/td/div/table/tbody/tr[2]/td[3]/table[1]/tbody/tr/td[2]/a["
                + str(int(numero_xpath_n_paginas)+5)
                + "]"
        )
        enlace_pagina_n_field = self.driver.find_element(By.XPATH, enlace_pagina_n_xpath)
        print(enlace_pagina_n_xpath)
        print("-------------")
        self.reutilizar.esperando_elemento_html(enlace_pagina_n_field)
        enlace_pagina_n_field.click()

    def descargar_pdfs_pagina_n(self, j, xpath_no_varia, enlace_pdf_varia):
        #creando lista q deseo obtener , ya despues le hago un update

        resultados_pagina_n = []

        #Descargando cada pdf fila por fila
        enlace_pdf_xpath = xpath_no_varia + str(j + 2) + enlace_pdf_varia
        enlace_pdf_field = self.driver.find_element(By.XPATH, enlace_pdf_xpath)
        self.reutilizar.esperando_elemento_html(enlace_pdf_field)
        enlace_pdf_field.click()

        #obteniendo num caja,fila por fila
        celda_numero_caja = "]/td[12]/div"
        celda_numero_caja = xpath_no_varia + str(j + 2) + celda_numero_caja
        celda_numero_caja = self.driver.find_element(By.XPATH, celda_numero_caja)
        self.reutilizar.esperando_elemento_html(celda_numero_caja)
        resultados_pagina_n.append(celda_numero_caja.text.strip())

        # obteniendo fecha emision ,fila por fila
        celda_fecha_emision = "]/td[7]/div"
        celda_fecha_emision = xpath_no_varia + str(j + 2) + celda_fecha_emision
        celda_fecha_emision = self.driver.find_element(By.XPATH, celda_fecha_emision)
        self.reutilizar.esperando_elemento_html(celda_fecha_emision)
        resultados_pagina_n.append(celda_fecha_emision.text.strip())

        # obteniendo estado comprobante
        celda_estado_comprobante = "]/td[13]/div/img"
        celda_estado_comprobante = xpath_no_varia + str(j + 2) + celda_estado_comprobante
        celda_estado_comprobante = self.driver.find_element(By.XPATH, celda_estado_comprobante)
        self.reutilizar.esperando_elemento_html(celda_estado_comprobante)
        # Obtener el atributo "title" del elemento y eliminar espacios en blanco
        lista_temporal=celda_estado_comprobante.get_attribute("title").split('-')
        # del lista_temporal[0]
        # del lista_temporal[1]
        # obtenemos la ultima posicion de la lista. Usamos strip para borrar espacios del elemento -1
        resultados_pagina_n.append(str(lista_temporal[-1]).strip())

        # obteniendo fecha ingreso -hora
        celda_fecha_ingreso_hora = "]/td[6]/div"
        celda_fecha_ingreso_hora = xpath_no_varia + str(j + 2) + celda_fecha_ingreso_hora
        celda_fecha_ingreso_hora = self.driver.find_element(By.XPATH, celda_fecha_ingreso_hora)
        self.reutilizar.esperando_elemento_html(celda_fecha_ingreso_hora)
        resultados_pagina_n.append(celda_fecha_ingreso_hora.text.strip())


        # obteniendo fecha serie-correlativo
        celda_serie_correlativo = "]/td[3]/div"
        celda_serie_correlativo = xpath_no_varia + str(j + 2) + celda_serie_correlativo
        celda_serie_correlativo = self.driver.find_element(By.XPATH, celda_serie_correlativo)
        self.reutilizar.esperando_elemento_html(celda_serie_correlativo)
        resultados_pagina_n.append(celda_serie_correlativo.text.strip())

        #para tener ordenada la lista de lista en forma de tabla
        #solo da errores y causa confusion al recorrer matriz o tabla
        #resultados_pagina_n.append('\n')

        #return lista_numero_cajas,lista_fecha_emisiones,lista_estado_comprobantes,lista_celda_fecha_ingreso_hora
        return resultados_pagina_n