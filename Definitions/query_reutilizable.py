from Definitions.querys import DatabaseManager

class ReutilizarQuery:
    def __init__(self):
        #self.configuracion=config
        pass

    def borrar_tabla(self,config, nombre_tabla):
        db_manager = DatabaseManager(config)
        db_manager.connect()
        rpta_bd = db_manager.delete_data_tabla_sin_where(nombre_tabla)
        if rpta_bd:
            print('SE ELIMINO TODA LA TABLA:', nombre_tabla)

        else:
            print('ERROR AL ELIMINAR TABLA:', nombre_tabla)
        # cerrar siempre conexion
        db_manager.disconnect()