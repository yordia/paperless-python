"""
otro triger q tras cada insert ,copie  a la tabla historial_tickets
   -- Inserta la fila eliminada en historial_tickets
	--    INSERT INTO historial_tickets (contenido_ticket, num_caja, actualizacion, creacion)
	--    VALUES (OLD.contenido_ticket, OLD.num_caja, OLD.actualizacion, OLD.creacion);
"""
import mysql.connector

class DatabaseManager:
    def __init__(self,config):
        # CONEXION BD, usar localhost o ip
        """
        self.host = '127.0.0.1'
        self.username = 'root'
        self.password = '123456'
        self.database = '0_sovos'
        self.connection = None
        """
        self.host = config.host_bd
        self.username = config.usuario_bd
        self.password = config.clave_bd
        self.database = config.base_datos
        self.connection = None

    def connect(self):
        self.connection = mysql.connector.connect(
            host=self.host,
            user=self.username,
            password=self.password,
            database=self.database
        )
        return self.connection

    def disconnect(self):
        if self.connection:
            self.connection.close()

    def execute_query(self, query, values=None):
        try:
            cursor = self.connection.cursor()
            if values:
                cursor.execute(query, values)
            else:
                cursor.execute(query)
            #print(query,'@@@@@',values)
            self.connection.commit()
            return True

        except mysql.connector.Error as e:
            print("Error al ejecutar query:", e)
            return False

    def insert_data(self, table, columns, values):
        query = f"INSERT INTO {table} ({', '.join(columns)}) VALUES ({', '.join(['%s'] * len(columns))})"
        #print(query, '@@@@@', values)
        return self.execute_query(query, values)

    def consultar_data(self, table, columns):
        #query = f"SELECT {', '.join(columns)} FROM {table} WHERE {condition_column1}='{condition_value1}' "
        query = f"SELECT {', '.join(columns)} FROM {table} "
        #print(query)
        cursor = self.connection.cursor()
        cursor.execute(query)
        #para los select no es neceario llamar al metodo execute_query ya que usara self.connection.commit() y dara error
        return cursor.fetchall()

    def update_data(self, table, columns, values, condition_column1, condition_value1, condition_column2,condition_value2):
        set_values = ', '.join([f"{column} = %s" for column in columns])
        query = f"UPDATE {table} SET {set_values} WHERE {condition_column1} LIKE %s AND {condition_column2} = %s"
        #VALUES CONTIENE LA DATA A INSERTAR Y LAS CONDICIONES DONDE SE INSERTARA ,ejemplo:  se insertara posicion 0 y 1['14', 'Aceptado con observaciones', '%2023-08-18%', 'F104 Nº 00026001']
        #donde se cumpla la condicion de fecha y folio en los wheres, POSICION 2 Y 3
        combined_values = values + [condition_value1, condition_value2]  # Asegurar que todos sean listas

        return self.execute_query(query, combined_values)

    def delete_data(self, table, condition, condition_values=None):
        query = f"DELETE FROM {table} WHERE {condition}"
        return self.execute_query(query, condition_values)
    def delete_data_tabla_sin_where(self, table):
        query = f"DELETE FROM {table}"
        return self.execute_query(query)

    def hola_mundo(self):
        print('1111111111')