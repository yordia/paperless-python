import sys
#sys.path.append(r'C:\users\pma0571\appdata\local\programs\python\python37\lib\site-packages\paramiko')

import os
import paramiko
import time

class DescargaLogMasivo:
    def __init__(self, config):
        self.var_usar_despues_en_otro_metodo = None
        self.config=config

        self.user_caja = config.user_caja
        self.password = config.passw_caja
        self.modo=config.modo
        self.descarga_log =config.descarga_log
        self.num_cajas = config.num_cajas
        self.ip_cajas = config.ip_cajas
        self.nombre_carpeta =config.nombre_carpeta_fecha_hora_seg_ms
        self.ruta_descarga_logs =config.ruta_descarga_logs#windows
        self.ruta_logs_cajas = config.ruta_logs_cajas #linux

    def tiempoEjecucion(self):
        # por defecto al ejecutar script de impresion,coomo se importa conexion ,se descargara el log de nuevo
        start_time = time.time()
        if self.descarga_log=='si':
            rpta=self.descarga_logs_masivo()
        end_time = time.time()
        # Cálculo del tiempo de ejecución
        execution_time = end_time - start_time
        print("El tiempo de descarga MASIVO,< fue de", execution_time, "segundos.")
        return rpta


    def reutilizarLogin(self,host):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=self.user_caja, password=self.password)
        session = client.get_transport().open_session()
        return  session,client

    # metodo para tener una copia de cualquier archivo por ssh
    def copiaArchivo(self,num_caja,ruta_linux,ruta_windows,ip):
        if self.descarga_log=='si':
            error=''
            try:
                #el metodo reutilizar login devuelve 2 variables pero solo usare el primero.
                session,client = self.reutilizarLogin(ip)
                sftp_client = client.open_sftp()
                #PODRIA CONCATENAR NUM CAJA COMO MEJORA
                sftp_client.get(ruta_linux,ruta_windows)
                # sftp_client.put('C:/ncr/apps/despliegue/prueba_copi/runtest.java', '0prueba3')
                sftp_client.close()
                session.close()
            except Exception as e:
                error='error'
                print(f'Error: {e} en caja: {self.config.obtener_num_caja(ruta_windows)}')
                #RECUERDO QUE ERA PARA Q AL  NO DESCARGAR LOG, ALMENOS EXISTA EL ARCHIVO VACIO LIMPIO.
                #self.crear_archivo_log_momentaneo(ruta_windows, '')

            finally:
                print('Se cero las conexiones ssh')

        if self.modo == 'dev'and error=='':
            print(f'MODO DEV,log de caja TRUE: {self.config.obtener_num_caja(ruta_windows)}')
        elif error=='error':
            print(f'Error al descargar log de caja numero FALSE: {self.config.obtener_num_caja(ruta_windows)}')
    def crear_archivo_log_momentaneo(nself,nombre_archivo, contenido):
        with open(nombre_archivo, 'w') as archivo:
            archivo.write(contenido)

    def descarga_logs_masivo(self):

        lista_num_cajas = self.num_cajas.split(',')
        lista_ip_cajas = self.ip_cajas.split(',')

        lista_ruta_cajas = []
        lista_nombre_logs_linux = []
        lista_ruta_windows=[]
        # crear carpeta con formato personalziado
        os.mkdir(self.ruta_descarga_logs)
        for i in range(len(lista_num_cajas)):
            lista_ruta_cajas.append(self.ruta_logs_cajas + lista_num_cajas[i] + '.LOG')
            elementos_cadena = lista_ruta_cajas[i].split('/')
            # LOS 2 PUNTOS DESPUES DEL -1 INDICAN QUE SE DEVUELVA UN ARREGLO O LISTA
            lista_nombre_logs_linux.append(str(elementos_cadena[-1]))
            lista_ruta_windows.append(self.ruta_descarga_logs + '\\' + lista_nombre_logs_linux[i])

        print(lista_ruta_cajas)
        print(lista_ip_cajas)
        print(lista_ruta_windows)

        for i in range(len(lista_num_cajas)):
            self.copiaArchivo(i + 1, lista_ruta_cajas[i], lista_ruta_windows[i], lista_ip_cajas[i])
        print('DESCARGAS finalizadas')
        return lista_ruta_windows

