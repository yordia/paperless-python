import os
import sys
from datetime import datetime

from Definitions.query_reutilizable import ReutilizarQuery
from Definitions.querys import DatabaseManager


class GuardarTicketsImpresosLog:

    def __init__(self,config):
        self.config =config
        self.tabla_ticket=config.tabla_tickets

        reutilizar_query=ReutilizarQuery()
        reutilizar_query.borrar_tabla(config,config.tabla_tickets)

    def procesar_varios_logs(self,lista_rutas):
        int_numero_caja =0
        resultado_de_una_impresion=[]
        for indice,ruta_archivo in enumerate(lista_rutas):
            #aqui por que no se inicializa en init??

            int_numero_caja = self.config.obtener_num_caja(lista_rutas[indice])
            if os.path.exists(ruta_archivo):  # Verificar si el archivo existe
                print(f"Procesando archivo: {ruta_archivo}")

                impresion_n=self.obtener_impresiones_log_n(ruta_archivo,int_numero_caja)
                #print('inicioooooo')
                for linea in impresion_n:
                    #convertimos <class 'datetime.datetime'> a string
                    linea=str(linea)
                    resultado_de_una_impresion.append(linea+'\n')
                    if "FINALIZA_TICKET" in linea:
                        un_solo_ticket=''.join(resultado_de_una_impresion)
                        #agregamos el insert a la bd
                        #print(un_solo_ticket)
                        self.insertar_bd(un_solo_ticket,int_numero_caja)
                        #se debe vaciar la lista para q a al sgte iteracion,
                        resultado_de_una_impresion=[]

            else:
                print(f"IMPORTANTE: Archivo log no encontrado, revisar la descarga de logs: {ruta_archivo}")
                #no es necesario debido a q no hay mas codigo despues de continue
                #continue


    def insertar_bd(self,impresion,num_caja):
        # Uso de la clase
        db_manager = DatabaseManager(self.config)
        db_manager.connect()

        # Inserción de datos
        # lista
        insert_columns = ['contenido_ticket','num_caja']
        insert_values = (impresion,num_caja)
        # si se inserta vacio (no null) significa q no existe en PDF
        rpta_bd = db_manager.insert_data(self.tabla_ticket, insert_columns, insert_values)

        if rpta_bd:
            pass
            #print('TICKET SE INSERTO CORRECTAMENTE')
        else:
            print('TICKET FALLO AL INSERTAR')
        # cerrar siempre conexion
        db_manager.disconnect()


    def obtener_impresiones_log_n(self,ruta_log,num_caja):
        with open(ruta_log,'r') as f:
            lineas = f.readlines()
            nuevo_ticket_impresion=True

            arreglo_linea_n=[]
            linea_impresion=None

            fecha_hora_inicia_log=None
            contador_linea_impresion=0
            tercera_linea_log=True

            impresion_n=[]
            for indice, line in enumerate(lineas):

                if tercera_linea_log:
                    contador_linea_impresion+=1
                    if contador_linea_impresion==3:
                        tercera_linea_log = False
                        fecha_hora_inicia_log=line.strip()
                        impresion_n.append(fecha_hora_inicia_log)
                        print('---'+fecha_hora_inicia_log+'---')

                if line.__contains__("printNormal:"):
                    if nuevo_ticket_impresion:
                        #print('INICIA_TICKET')
                        impresion_n.append('INICIA_TICKET')
                        nuevo_ticket_impresion = False

                    arreglo_linea_n=line.split('printNormal:')
                    linea_impresion=arreglo_linea_n[-1].strip()

                    if linea_impresion.__contains__('8lF'):
                        impresion_n.append('FINALIZA_TICKET')
                        #print('FINALIZA_TICKET')
                        nuevo_ticket_impresion=True
                        continue


                    linea_impresion = linea_impresion.replace('\n', '')
                    linea_impresion = linea_impresion.replace('|bC', '')
                    linea_impresion = linea_impresion.replace('|N', '')
                    impresion_n.append(linea_impresion)
                if indice == len(lineas) - 1:  # Última línea
                    print('ULTIMA_HORA_EJECUCION_CAJA-',num_caja)
                    impresion_n.append('ULTIMA_HORA_EJECUCION_CAJA-'+str(num_caja))
                    print(indice + 1, self.obtener_ultima_fecha_hora(line))
                    impresion_n.append(self.obtener_ultima_fecha_hora(line))
                    print('*******************************************')
                    print()

        return impresion_n

    def obtener_ultima_fecha_hora(self,linea):
        fecha_hora=None
        #print(type(fecha_hora))
        try:
            fecha_hora = datetime.strptime(linea[:19], "%Y-%m-%d %H:%M:%S")
            """
            if ultima_fecha_hora is None or fecha_hora > ultima_fecha_hora:
                ultima_fecha_hora = fecha_hora
            """
        except ValueError:
            pass  # Ignorar líneas que no tienen el formato de fecha y hora
        return fecha_hora

