import re

from Definitions.query_reutilizable import ReutilizarQuery
from Definitions.querys import DatabaseManager

class InsertarTablaLog:
    def __init__(self,config):
        # borrando tabla BD
        self.ajustes = config
        self.tabla_log_cajas_ljvm = config.tabla_log_cajas_ljvm
        reutilizar_query = ReutilizarQuery()
        reutilizar_query.borrar_tabla(config, config.tabla_log_cajas_ljvm)

    def principal(self):
        db_manager = DatabaseManager(self.ajustes)
        db_manager.connect()
        data_limpia_tabla_tickes=self.obtener_data()
        for element in data_limpia_tabla_tickes:
            if element=='NO_SE_VALIDA_CON_SOVOS':
                pass
            else:
                print(element)
                # Inserción de datos
                # lista
                insert_columns = [ 'igv','op_gravada','importe_total','tipo_doc','serie_correlativo','fecha_emision','num_caja']
                insert_values = (element[0],element[1],element[2],element[3],element[4],element[5],element[6])
                #print('####',element[0],element[1],element[2],element[3],element[4],element[5],element[6])
                rpta_bd = db_manager.insert_data("solodeundia_logs_cajas", insert_columns, insert_values)

                if rpta_bd:
                    pass
                    #print('SE INSERTO TABLA-LOG')
                else:
                    print('ERROR AL INSERTAR TABLA_LOG')
                # cerrar siempre conexion
        db_manager.disconnect()
        #print(data_limpia_tabla_tickes)

    def obtener_data(self):
        resultados_extraido_tabla_tickes = []

        base_de_datos = DatabaseManager(self.ajustes)
        base_de_datos.connect()

        columns = ['contenido_ticket', 'num_caja']
        condition_column1='num_caja'
        condition_value1=1
        result = base_de_datos.consultar_data('tickets',columns)

        if result==[]:
            print("No hay resultados")
        else:
            print('Existen resultados')
            contador = 0


            for tupla in result:
                """
                print(row)
                print(len(row))
                print(type(row))
                """
                contador += 1
                cadena = '\n'.join(tupla)
                lista=cadena.split('\n')
                #print(lista)
                rpta=self.buscar_texto_e_insertar_tabla_log(lista)
                #no agregamos la venta asistida/mayorista (contigencia solo es para mayorista), contigencia no llega a sovos
                if rpta[3]=='NO_EXISTE_TIPO_DOC' and rpta[4]=='NO_EXISTE_SER_CORR'and rpta[5]=='NO_EXISTE_FECHA_EMISION':
                    #esto quiere decir que no es una boleta,factura o nota credito
                    resultados_extraido_tabla_tickes.append('NO_SE_VALIDA_CON_SOVOS')
                    pass
                else:
                    resultados_extraido_tabla_tickes.append(rpta)
            print('contadooooo',contador)
        return  resultados_extraido_tabla_tickes
    def contador(self,contador):
        contador += 1


    def buscar_texto_e_insertar_tabla_log(self,lista):
        num_caja='NO_EXISTE_NUM_CAJA'
        tipo_doc = 'NO_EXISTE_TIPO_DOC'
        folio = 'NO_EXISTE_SER_CORR'
        fecha_emision = 'NO_EXISTE_FECHA_EMISION'


        igv = 'NO_EXISTE_IGV'
        op_gravada = 'NO_EXISTE_OP_GRAVADA'
        importe_total = 'NO_EXISTE_IMPORTE_TOTAL'

        redondeo_keyword = 'REDONDEO'
        soles_pagados_keyword = 'TOTAL CONTADO'
        op_inafecta_keyword = 'OP. INAFECTA'

        #lista = ["Hola mundo", "Este es un ejemplo", "Otra línea"]
        for posicion, linea in enumerate(lista):
            #if re.search("^H", linea):
            if linea.__contains__('I.G.V.(18%)'):
                elementos_linea = linea.strip().split(' ')
                if len(elementos_linea) == 1:
                    igv = 'IGV en pdf sovos vacio'
                else:
                    igv = elementos_linea[-1]
            #IMPORTE TOTAL
            if linea.__contains__('IMPORTE TOTAL'):
                elementos_linea = linea.strip().split(' ')
                if len(elementos_linea) == 2:
                    importe_total ='IMPORTE TOTAL en pdf sovos esta vacio'
                else:
                    importe_total = elementos_linea[-1]

            if linea.__contains__('OP. INAFECTA'):
                #print(linea)
                pass
            if linea.__contains__('OP. GRAVADA'):
                elementos_linea = linea.strip().split(' ')
                if len(elementos_linea) == 2:
                    op_gravada = 'OP. GRAVDA en pdf sovos esta vacio'
                else:
                    op_gravada = elementos_linea[-1]


            if linea.strip().__contains__('BOLETA DE VENTA ELECTRONICA'):
                tipo_doc = linea.strip()
            elif linea.strip().__contains__('FACTURA DE VENTA ELECTRONICA'):
                tipo_doc = linea.strip()
            elif linea.strip().__contains__('NOTA DE CREDITO ELECTRONICA'):
                tipo_doc =linea.strip()

            if (linea.strip().__contains__('B104') or linea.strip().__contains__('F104')) and not linea.strip().__contains__(':'):
                folio=linea.strip()
            if linea.strip().__contains__('Fecha de emisión'):
                elementos_linea = linea.strip().split(' ')
                fecha_emision = elementos_linea[-3]

                #(solucion momentanea) para q no arroje error al comparar vs la data de la web sovos
                fecha_emision = fecha_emision.replace('/23', '-2023')
                fecha_emision = fecha_emision.replace('/24', '-2024')
                fecha_emision = fecha_emision.replace('/25', '-2025')
                fecha_emision = fecha_emision.replace('/26', '-2026')
                fecha_emision = fecha_emision.replace('/27', '-2027')
                fecha_emision = fecha_emision.replace('/28', '-2028')
                fecha_emision = fecha_emision.replace('/29', '-2029')
                fecha_emision = fecha_emision.replace('/30', '-2030')
                fecha_emision = fecha_emision.replace('/31', '-2031')
                fecha_emision = fecha_emision.replace('/32', '-2032')
                fecha_emision = fecha_emision.replace('/33', '-2033')
                fecha_emision = fecha_emision.replace('/', '-')


            if posicion==len(lista)-1:
                num_caja=linea

        #print(igv,op_gravada,importe_total,tipo_doc,folio,fecha_emision,num_caja)
        lista_extraida_tabla_tickets=[igv,op_gravada,importe_total,tipo_doc,folio,fecha_emision,num_caja]
        return lista_extraida_tabla_tickets
        #AGREGAR TODO A UNA LISTA DE LISTAS, para luego insertarlo a tabla b en otro metodo



