from selenium import webdriver
from selenium.webdriver.chrome.service import Service

class DriverChrome:
    def __init__(self,config):
        # Especifica la ruta del ChromeDriver
        self.chrome_driver_path = "a:\\116.0.5845.111.chromedriver.exe"
        # Configurar el servicio del ChromeDriver
        chrome_service = Service(self.chrome_driver_path)

        # Configuración del navegador
        options = webdriver.ChromeOptions()
        options.add_argument('--start-maximized')
        # Establecer el directorio de descarga por defecto, disco D
        prefs = {'download.default_directory': config.ruta_descarga_pdf}
        options.add_experimental_option('prefs', prefs)
        # options.add_argument('--headless')  # Ejecutar en modo sin cabeza
        # options.add_argument('--disable-gpu')  # Deshabilitar aceleración de hardware (puede ser necesario)
        # Inicializar el WebDriver con el servicio q trae la ruta del driver y las opciones de descarga y otros ajustes.
        self.driver = webdriver.Chrome(service=chrome_service, options=options)

