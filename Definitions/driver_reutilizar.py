from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

class ReutilizarDriver:

    def __init__(self,driver,config):
        self.driver = driver
        self.config = config

    def esperando_elemento_html(self, elemento_web):
        WebDriverWait(self.driver, self.config.tiempo_espera_global_elemento_html).until(EC.element_to_be_clickable(elemento_web))

