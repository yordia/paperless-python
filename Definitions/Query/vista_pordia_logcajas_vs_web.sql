SELECT a1.serie_correlativo AS FOLIO,

a1.fecha_emision AS FECHA,
(CASE b1.op_gravada WHEN a1.op_gravada  THEN CONCAT('OK: ',a1.op_gravada)          ELSE CONCAT('Caja: ',b1.op_gravada,' | WebP: ',a1.op_gravada) END) AS  OP_GRAVADA ,
(CASE b1.igv WHEN a1.igv  THEN CONCAT('OK: ',a1.igv)                               ELSE CONCAT('Caja: ',b1.igv,' | WebP: ',a1.igv) END) AS IGV ,
(CASE b1.importe_total WHEN a1.importe_total  THEN CONCAT('OK: ',a1.importe_total) ELSE CONCAT('Caja: ',b1.importe_total,' | WebP: ',a1.importe_total) END) AS TOTAL ,
(CASE b1.tipo_doc WHEN a1.tipo_doc  THEN CONCAT('OK: ',a1.tipo_doc)                ELSE CONCAT('Caja : ',b1.tipo_doc,' | WebP: ',a1.tipo_doc) END) AS TIPO_DOC,
(CASE b1.serie_correlativo WHEN a1.serie_correlativo  THEN CONCAT('OK: ',a1.serie_correlativo) ELSE CONCAT('Caja: ',b1.serie_correlativo,' | WebP: ',a1.serie_correlativo) END) AS SER_COR,
a1.num_caja as NUM_CAJA1,


(case  when concat(Mensaje0,Mensaje1,Mensaje2,Mensaje3)='****'	then CONCAT('CORRECTO') ELSE
(CASE WHEN a1.serie_correlativo=b1.serie_correlativo THEN  'INCORRECTO'  ELSE  'NO EXISTE W.SOVOS,SI EXISTE EN LOG CAJA'  END) END) AS ESTADO

#(CASE a1.num_caja WHEN b1.num_caja  THEN 'TRUE' ELSE CONCAT('Caja: ',b1.num_caja,' | WebP: ',a1.num_caja) END) AS NUM_CAJA2
FROM datapordia_logs_cajas a1
LEFT JOIN(
SELECT '*' AS Mensaje0,serie_correlativo AS folio,fecha_emision_comparar,op_gravada,igv,importe_total,tipo_doc,serie_correlativo,num_caja
FROM datapordia_pdf_web_sovos
)b1 ON  b1.folio=a1.serie_correlativo
/*LEFT JOIN (
SELECT  '*' AS mensaje, serie_correlativo as folio,fecha_emision_comparar  FROM datapordia_pdf_web_sovos
)c1  ON	c1.folio=a1.serie_correlativo AND  c1.fecha_emision_comparar=a1.fecha_emision
*/
LEFT JOIN (
SELECT  '*' AS Mensaje1, serie_correlativo as folio,fecha_emision_comparar,op_gravada  FROM datapordia_pdf_web_sovos
)d1  ON	d1.folio=a1.serie_correlativo AND  d1.fecha_emision_comparar=a1.fecha_emision AND d1.op_gravada=a1.op_gravada

LEFT JOIN (
SELECT  '*' AS Mensaje2, serie_correlativo as folio,fecha_emision_comparar,op_gravada,igv  FROM datapordia_pdf_web_sovos
)e1  ON	d1.folio=a1.serie_correlativo AND  e1.fecha_emision_comparar=a1.fecha_emision AND e1.op_gravada=a1.op_gravada AND e1.igv=a1.igv

LEFT JOIN (
SELECT  '*' AS Mensaje3, serie_correlativo as folio,fecha_emision_comparar,op_gravada,igv,importe_total  FROM datapordia_pdf_web_sovos
)f1  ON	d1.folio=a1.serie_correlativo AND  f1.fecha_emision_comparar=a1.fecha_emision AND f1.op_gravada=a1.op_gravada AND f1.igv=a1.igv AND f1.importe_total=a1.importe_total

GROUP BY a1.serie_correlativo
ORDER BY a1.serie_correlativo ASC