import os
from datetime import datetime

class Config:
    def __init__(self):
        # self.base_datos = '0_sovos'
        self.base_datos = 'sovos_pruebas'
        self.fecha_inicio = '14-09-2023'
        self.fecha_fin = '14-09-2023'
        self.caja = '10'
        modo_prueba_humo='si'

        if modo_prueba_humo=='si':
            self.filtra_caja = True
            self.num_cajas = '010'
            self.ip_cajas = '10.166.218.241'
        else:
            # por mejorar a formato diccionario o lo otroseria tener en una tabla la lista.
            self.filtra_caja = False
            self.num_cajas = '001,002,003,004,005,006,007,008,009,010,011,012,013,014,015,016,017,018'
            self.ip_cajas = '10.166.218.232,10.166.218.233,10.166.218.234,10.166.218.235,10.166.218.236,10.166.218.237,10.166.218.238,10.166.218.239,10.166.218.240,10.166.218.241,10.166.218.242,10.166.218.243,10.166.218.244,10.166.218.245,10.166.218.246,10.166.218.247,10.166.218.248,10.166.218.249'

        """pruebas con multiplo de 10, tiene 60 pdfs
        self.caja = '13'
        self.fecha_inicio = '06-09-2023'
        self.fecha_fin = '07-09-2023'
        """

        # solo usar valores dev y prd
        self.modo='dev'
        #solo usar valores si y no
        self.descarga_log='si'

        self.ruc = '201090721772'
        #https://account-test.sovos.com/es-ES
        #REGISTRO PAULA
        #https://ereceipt-pe-s01-uat.sovos.com/Facturacion/jsp/inicio/menuInicio.jsp

        #https://ereceipt-pe-s01-uat.sovos.com/Facturacion/jsp/sso/sovos-inicial-login.jsp
        #yordiaguilaroyolo@gmail.com
        #55355674aA@
        self.username_antiguo= 'admin'
        self.password_antiguo = 'abc123'
        self.username = 'yordiaguilaroyolo@gmail.com'
        self.password = '55355674aA@'
        self.login_url_antiguo = 'https://asp4demos.paperless.com.pe/Facturacion/login.jsp'
        self.login_url = 'https://ereceipt-pe-s01-uat.sovos.com/Facturacion/jsp/sso/sovos-inicial-login.jsp'
        #link de renzo
        #"http://egw401qa.paperless.com.pe/Facturacion/login.jsp?error=base.expirado"
        self.login_url_antiguo_sin_ssl='http://egw401qa.paperless.com.pe/Facturacion/login.jsp'


        disco_usar = 'B'

        #PROBLEMA CON LOS TRIGERS
        fecha_hora_actual = datetime.now()
        self.nombre_carpeta_fecha_hora_seg_ms = fecha_hora_actual.strftime("%Y-%m-%d_%H-%M-%S-%f")
        self.host_bd='127.0.0.1'
        self.usuario_bd='root'
        self.clave_bd='123456'


        self.reprocesar = 'no'
        """
        if self.reprocesar=='si':
            #ESTE REPROCESAR DESCAGARA PDF en la carpeta reprocesar
            #conexion a otra BD, para q no afecte a los trigers
            self.base_datos = 'sovos_reprocesar'
            #self.fecha_inicio = '05-09-2023'
            #self.fecha_fin = '05-09-2023'

            #reprocesa por un dia
            self.ruta_descarga_logs = f'{disco_usar}:\\ncr\\reprocesar\\01copiar-logs-un-dia'
            self.ruta_descarga_pdf = f'{disco_usar}:\\ncr\\reprocesar\\01pdf-un-dia\\{self.nombre_carpeta_fecha_hora_seg_ms}'
            #POR MEJORAR EL REPROCESO (01pdf-un-dia, debe funsionar sin interner) con BD LOCAL.

            #no es necesario crearlas ya los archivos pdf y log seran copiados a la carpeta de arriba
            self.creacion_carpeta_pdf(self.ruta_descarga_pdf,'PDF')
            #self.creacion_carpeta_logs(self.nombre_carpeta_fecha_hora_seg_ms,'LOG')
        else:
        """
        self.ruta_descarga_pdf = f'{disco_usar}:\\ncr\\pdf_descargados\\{self.nombre_carpeta_fecha_hora_seg_ms}'
        # funciona con ambas formas \\ barra invertida o barra normal /
        self.ruta_descarga_logs = f'{disco_usar}:\\ncr\\log_cajas\\{self.nombre_carpeta_fecha_hora_seg_ms}'
        #Creamos las carpetas con fecha y hora para los pdf
        self.creacion_carpeta_pdf(self.nombre_carpeta_fecha_hora_seg_ms,'PDF')

        self.nombre_pdf_ruc='20109072177'
        self.nombre_pdf_separacion='_'

        self.nombre_pdf_digito_boleta='3'
        self.nombre_pdf_digito_factura='1'
        self.nombre_pdf_digito_nota_credito='7'
        self.nombre_pdf_serie_boleta = 'B104-'
        self.nombre_pdf_serie_factura = 'F104-'
        self.nombre_pdf_serie_nota_credito = 'B104-'
        #Este es el unico tiempo q es dinamico, cada cierto intervalo de tiempo se consulta el elemento , como maximo durante 5 seg.
        self.tiempo_espera_global_elemento_html =7
        #tiempo espera entre paginas webs (estos 2 tiempos se tienen q hacer dinamicos) PENDIENTE MEJORA-01
        self.tiempo_espera_paginas_web_lentas=7
        self.tiempo_espera_paginas_web_rapidas=3

        #PENDIENTE MEJORA-02 que se realice
        self.tiempo_espera_ultimo_pdf_descargado=2.75

        self.tienda = 'T104 - T104'


        #parametros cuando pagina sea 500 en 500, si da tiempo
        #6 a 14 y 7 a 16...
        self.numero_filas=500
        self.numero_xpath_n_paginas=500
        self.numero_xpath_n_paginas_pag2_en_adelante=502

        #parametros para descarga log:
        self.user_caja='reg'
        self.passw_caja='cencopos'


        self.ruta_logs_cajas =  'gd90/L_JVM'

        #SOLO BD
        self.tabla_tickets='tickets'
        self.tabla_log_cajas_ljvm='solodeundia_logs_cajas'
        self.tabla_web_sovos='solodeundia_pdf_web_sovos'



    # se deberia reutilizar a futuro para crear carpetas logs
    def creacion_carpeta_pdf(self,nombre_carpeta, descripccion):
        # Nombre de la carpeta que deseas crear

        # Ruta completa donde se creará la carpeta (puedes ajustarla según tus necesidades)
        #os.getcwd()--devuelve el directorio actual donde se ejecuta el script.

        #ruta_completa = os.path.join(os.getcwd(), nombre_carpeta)
        # Intenta crear la carpeta
        try:
            os.mkdir(self.ruta_descarga_pdf)
            print(f"Carpeta '{nombre_carpeta}' creada para la descarga de {descripccion} en '{self.ruta_descarga_pdf}'.")
        except FileExistsError:
            print(f"La carpeta '{nombre_carpeta}' ya existe en '{self.ruta_descarga_pdf}'.")
        except Exception as e:
            print(f"Ocurrió un error al crear la carpeta: {e}")

    def obtener_num_caja(self, ruta_de_una_caja):
        # De esta forma tomatmos el numero caja 001,002,etc
        lista_numero_caja = ruta_de_una_caja.split('L_JVM')[1].split('.')[0]
        int_numero_caja = int(lista_numero_caja)
        return int_numero_caja


#caja 1 del 20-08-2023 --> 15 pdfs
#16-08-2023 -->198 pdfs.
#17-08-2023 -->68 pdfs.
#18-08-2023 solo 2 pdfs.
#19 agosto -> 365 pdf

#13-08 al 15-08 --> 8 pdf
#12-08 al 15-08 --> 109 pdf
#11-08 al 15-08 --> 201 pdf
