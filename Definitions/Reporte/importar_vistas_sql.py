from datetime import datetime

import openpyxl

from Definitions.config import Config
from Definitions.querys import DatabaseManager

class GeneraReporteExcel:
    def __init__(self,config):
        self.config=config
        pass
    def generar_reporte(self):
        # se debe crear el objeto DatabaseManager, en hola_mundo no es necesario
        db_manager = DatabaseManager(self.config)
        conexion = db_manager.connect()
        var_cursor = conexion.cursor()

        # nombre_vista1='a_web_vs_cajas'
        # nombre_vista2='a_cajas_vs_web'
        # nombre_vista3='a_repetidos_logs_cajas'
        # nombre_vista4='a_repetidos_web_sobos'

        nombre_vista1 = 'solodeundia_web_vs_logcajas'
        nombre_vista2 = 'solodeundia_logcajas_vs_web'

        nombre_vista3 = 'solodeundia_repetidos_logs_cajas'
        nombre_vista4 = 'solodeundia_repetidos_pdf_web_sovos'

        # Creamos un libro de trabajo y una hoja
        libro_trabajo = openpyxl.Workbook()
        hoja = libro_trabajo.active
        hoja.title = 'Web Sobos VS Log Cajas'
        self.refactoriza_vistas_comparacion(hoja, nombre_vista1, var_cursor)

        # al crearla de este modo, tambien se activa la hoja 2
        hoja2 = libro_trabajo.create_sheet(title='Log Cajas VS Web Sovos')
        self.refactoriza_vistas_comparacion(hoja2, nombre_vista2, var_cursor)

        # al crearla de este modo, tambien se activa la hoja 3
        hoja3 = libro_trabajo.create_sheet(title='Folios duplicados en Log Cajas')
        self.refactoriza_vista_folios_duplicados(hoja3, nombre_vista3, var_cursor)

        # al crearla de este modo, tambien se activa la hoja 4
        hoja4 = libro_trabajo.create_sheet(title='Folios duplicados en Web Sovos')
        self.refactoriza_vista_folios_duplicados(hoja4, nombre_vista4, var_cursor)

        fecha_hora_actual = datetime.now()
        nombre_excel_fecha_hora_seg_ms = fecha_hora_actual.strftime("%Y-%m-%d__%H-%M-%S")

        #cuando se ejecuta desde esta ruta A:\paperless14agosPY\Definitions\Reporte\importar_vistas_sql.py
        #libro_trabajo.save(f'../../Support/xborrar/{nombre_excel_fecha_hora_seg_ms}.xlsx')
        # cuando se ejecuta desde archivo main.py
        libro_trabajo.save(f'./xborrar/{nombre_excel_fecha_hora_seg_ms}.xlsx')

        var_cursor.close()
        conexion.close()


    def refactoriza_vistas_comparacion(self,hoja, nombre_vista, cursor):
        # Agregamos una cabecera a la hoja
        hoja.append(['FOLIO', 'ESTADO', 'F.EMISION', 'OP.GRAVADA', 'IGV', 'TOTAL', 'TIPO.DOC', 'FOLIO', 'NUM.CAJA'])

        # primer for para color fondo
        for celda in hoja[1]:
            celda.fill = openpyxl.styles.PatternFill(start_color='70ad47', end_color='70ad47', fill_type='solid')
        # segundo for para color texto
        for celda in hoja[1]:
            celda.font = openpyxl.styles.Font(color='FFFFFF', bold=True)

        # Agregamos filtros a la cabecera
        hoja.auto_filter.ref = 'A1:I1'

        cursor.execute(
            "SELECT FOLIO,ESTADO, FECHA, OP_GRAVADA, IGV,TOTAL,TIPO_DOC,SER_COR,NUM_CAJA1 FROM " + nombre_vista + ";")
        datos = cursor.fetchall()

        # Insertamos los datos en la hoja
        for i, fila in enumerate(datos, start=2):
            hoja.append(fila)

            if i % 2 == 0:
                # Si el índice de la fila es par, aplicamos un color de fondo
                for celda in hoja[i]:
                    celda.fill = openpyxl.styles.PatternFill(start_color='e2efda', end_color='e2efda', fill_type='solid')

        # Ajustamos el ancho por defecto de las columns
        hoja.column_dimensions['A'].width = 23
        hoja.column_dimensions['B'].width = 29
        hoja.column_dimensions['C'].width = 29
        hoja.column_dimensions['D'].width = 29
        hoja.column_dimensions['E'].width = 29
        hoja.column_dimensions['F'].width = 29
        hoja.column_dimensions['G'].width = 29
        hoja.column_dimensions['H'].width = 29
        hoja.column_dimensions['i'].width = 29
        hoja.column_dimensions['J'].width = 29
        print('SE CREO LA HOJA EXCEL')
        print(hoja)
        # return hoja


    def refactoriza_vista_folios_duplicados(self,hoja, nombre_vista, cursor):
        # Agregamos una cabecera a la hoja
        hoja.append(
            ['CANT_DUPLICADOS', 'NUM_CAJA', 'TIPO_DOCUMENTO', 'FOLIO', 'FECHA_EMISION', 'OP_GRAVADA', 'IGV', 'TOTAL',
             'LOG_DEL_DIA'])

        # primer for para color fondo
        for celda in hoja[1]:
            celda.fill = openpyxl.styles.PatternFill(start_color='70ad47', end_color='70ad47', fill_type='solid')
            # color texto
            celda.font = openpyxl.styles.Font(color='FFFFFF', bold=True)

        # Agregamos filtros a la cabecera
        hoja.auto_filter.ref = 'A1:I1'

        cursor.execute("SELECT * FROM " + nombre_vista + ";")
        datos = cursor.fetchall()

        # Insertamos los datos en la hoja
        for i, fila in enumerate(datos, start=2):
            hoja.append(fila)

            if i % 2 == 0:
                # Si el índice de la fila es par, aplicamos un color de fondo
                for celda in hoja[i]:
                    celda.fill = openpyxl.styles.PatternFill(start_color='e2efda', end_color='e2efda', fill_type='solid')

        # Ajustamos el ancho por defecto de las columns
        hoja.column_dimensions['A'].width = 23
        hoja.column_dimensions['B'].width = 29
"""
config=Config()
generar_reporte=GeneraReporteExcel(config)
generar_reporte.generar_reporte()
"""